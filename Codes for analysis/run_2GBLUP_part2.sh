#!/bin/sh
#SBATCH --time=0-6:00:00
#SBATCH --mem=60000
#SBATCH --cpus-per-task=1

#make phenotype file and put it in the r_sequence directory.phenotype1.prn for ASReml, and phenotype2.prn for MixBLUP
cd r_sequence
cp phenotype2.prn ./QTL/0
cd ./QTL/0
mv phenotype2.prn phenotype.txt
cp phenotype.txt ../1
cd ../../
mv phenotype1.prn phenotype.txt



cd ./QTL/big/ASReml
rm G.grm
mv G_asreml.giv G2_asreml.giv 

cd ../MixBLUP
rm G.grm

cd ../../

cd 0
sbatch run_all.sh 

cd ../0.05
sbatch run_all.sh 

cd ../0.1
sbatch run_all.sh 

cd ../0.15
sbatch run_all.sh 

cd ../0.2
sbatch run_all.sh 

cd ../0.4
sbatch run_all.sh 

cd ../0.6
sbatch run_all.sh 

cd ../0.7
sbatch run_all.sh 

cd ../0.8
sbatch run_all.sh 

cd ../0.85
sbatch run_all.sh 

cd ../0.9
sbatch run_all.sh 

cd ../0.95
sbatch run_all.sh 

cd ../0.99
sbatch run_all.sh 

cd ../
mkdir 1_2
cd 1
QTL=$(head -n 1 calc_grm.inp)

cd ../1_2
cp /home/WUR/yang110/files/1/* .
awk -v mrk_num="$QTL" 'NR==1{$1=mrk_num}1' calc_grm.inp > temp && mv temp calc_grm.inp
sbatch run_all.sh




