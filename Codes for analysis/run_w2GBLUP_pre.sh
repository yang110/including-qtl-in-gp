#!/bin/sh
#SBATCH --time=0-1:00:00
#SBATCH --mem=60000
#SBATCH --cpus-per-task=1


cd r_sequence/QTL

awk '{print NR, $2}' var_sorted_cumm_perc.txt > var.txt


for i in 0.05 0.1 0.15 0.2 0.4 0.6 0.7 0.8 0.85 0.9 0.95 0.99 1_2
do
cd ${i}
cp /home/WUR/yang110/files/w_2GBLUP/run_w2GBLUP.sh .
sbatch run_w2GBLUP.sh
cd ../
done
