#!/bin/sh
#SBATCH --time=9-7
#SBATCH --mem=60000
#SBATCH --cpus-per-task=1

cd r_sequence/QTL
cat genotype.txt | cut -c10- > geno.txt
sed -i 's/ //g' geno.txt
sed -E 's/(.)/\1,/g' geno.txt > geno.csv
rm geno.txt
head -12500 geno.csv > QTL_train.csv
tail -2500 geno.csv > QTL_test.csv
rm geno.csv

for i in 0.05 0.1 0.15 0.2 0.4 0.6 0.7 0.8 0.85 0.9 0.95 0.99 1_2; do number=$(head -n 1 $i/calc_grm.inp | grep -oP '\d+'); echo "$number" >> QTLnumber.txt; done

for i in 0 0.05 0.1 0.15 0.2 0.4 0.6 0.7 0.8 0.85 0.9 0.95 0.99 1_2
do
cd $i
mkdir SVM RF
cd SVM
cp /home/WUR/yang110/files/$i/SVM/* .
cd ../RF
cp /home/WUR/yang110/files/$i/RF/* .
cd ../../
done


cd ../
cat genotype.txt | cut -c8- > geno.txt
sed -E 's/(.)/\1,/g' geno.txt > geno.csv
rm geno.txt
head -12500 geno.csv > SNP_train.csv
tail -2500 geno.csv > SNP_test.csv
rm geno.csv

cd QTL

for i in 0 0.05 0.1 0.15 0.2 0.4 0.6 0.7 0.8 0.85 0.9 0.95 0.99 1_2
do
cd $i
cd SVM
sbatch run_python.sh
cd ../RF
sbatch run_python.sh
cd ../../
done



