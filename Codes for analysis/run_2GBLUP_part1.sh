#!/bin/sh
#SBATCH --time=0-6:00:00
#SBATCH --mem=60000
#SBATCH --cpus-per-task=1

#simulation
/home/WUR/yang110/QMSim_Linux/QMSim sequence.txt

#after that, the r_sequence directory generated

cd r_sequence
/home/WUR/yang110/programs/recode_genotypes
#randomly select 2500 training animals from each of the generations 11-15
awk '{print $1}' SNP_genotypes.txt | head -25000 > 1115num.txt
head -5000 1115num.txt | shuf -n 2500 | sort -n > rd_1115.txt
head -10000 1115num.txt | tail -5000 | shuf -n 2500 | sort -n >> rd_1115.txt
head -15000 1115num.txt | tail -5000 | shuf -n 2500 | sort -n >> rd_1115.txt
head -20000 1115num.txt | tail -5000 | shuf -n 2500 | sort -n >> rd_1115.txt
tail -5000 1115num.txt | shuf -n 2500 | sort -n >> rd_1115.txt
#write IDs of animals per validation generation to file
awk '{print $1}' SNP_genotypes.txt | tail -25000 > young.txt
head -5000 young.txt > 16num.txt
tail -5000 young.txt > 20num.txt
head -10000 young.txt | tail -5000 > 17num.txt
head -15000 young.txt | tail -5000 > 18num.txt
head -20000 young.txt | tail -5000 > 19num.txt
#randomly sample 500 animals per validation generation A(16-20)
shuf -n 500 16num.txt > rd_1620.txt
shuf -n 500 17num.txt >> rd_1620.txt
shuf -n 500 18num.txt >> rd_1620.txt
shuf -n 500 19num.txt >> rd_1620.txt
shuf -n 500 20num.txt >> rd_1620.txt
sort -n rd_1620.txt > rd_nr.txt
#select genotypes 12500 training animals
awk 'NR==FNR{a[$0]=$0}NR>FNR{if($1==a[$1])print $0}' rd_1115.txt SNP_genotypes.txt > proven_sort_genotype.txt
awk 'NR==FNR{a[$0]=$0}NR>FNR{if($1==a[$1])print $0}' rd_nr.txt SNP_genotypes.txt > young_sort_genotype.txt
cat proven_sort_genotype.txt young_sort_genotype.txt > genotype.txt

#now we have genotype.txt
#get the ID of animals 

awk '{print $1}' genotype.txt > num.txt


# make phenotype files, for ASReml, 3 columns, with *; for Mixblup, delete the animals without phenotype, and add a column with 1
awk '{print $1,$10}' p1_data_001.txt > phen.txt
awk 'NR==FNR{a[$0]=$0}NR>FNR{if($1==a[$1])print $0}' num.txt phen.txt | head -n12500 > phenotype_train.txt
awk 'NR==FNR{a[$0]=$0}NR>FNR{if($1==a[$1])print $0}' num.txt phen.txt | tail -n2500 > phenotype.txt
awk '{print NR, $0," 1"}' phenotype_train.txt > phenotype2.prn 
awk '{print NR, $0}' phenotype_train.txt > phenotype1.prn

mkdir QTL
cp p1_freq_qtl_001.txt ./QTL
cd QTL
echo "$(tail -n +2 p1_freq_qtl_001.txt)" > p1_freq_qtl_001.txt


awk '{print $1, $4}' p1_freq_qtl_001.txt > variance.txt
sort -n -k2 -r variance.txt > var1.txt
rm variance.txt
mv var1.txt variance.txt
#Remove "Q" from first column
sed 's/Q//' variance.txt > var_sorted.txt
#Compute the sum of the QTL variances
awk -F' ' '{sum+=$2;}END{print sum;}' var_sorted.txt
#add a column with the cumulative variance
awk '{print $1,$2,m1=$2+m1}' var_sorted.txt >var_sorted_cumm.txt

#now get the result and handling phenotype file

mkdir big
cd big
mkdir MixBLUP
mkdir ASReml
cp /home/WUR/yang110/files/big/MixBLUP/* ./MixBLUP
cp /home/WUR/yang110/files/big/ASReml/* ./ASReml # run ASReml first

cd ../../
tail p1_freq_mrk_001.txt


mrk_num=$(awk 'END{gsub(/^M/, "", $1); print $1}' lm_mrk_001.txt)
echo "The number of SNP is $mrk_num"

cd QTL/big/ASReml
ln -s ../../../genotype.txt
awk -v mrk_num="$mrk_num" 'NR==1{$1=mrk_num}1' calc_grm.inp > temp && mv temp calc_grm.inp
sbatch run_calc_grm.sh

cd ../MixBLUP
ln -s ../../../genotype.txt
awk -v mrk_num="$mrk_num" 'NR==1{$1=mrk_num}1' calc_grm.inp > temp && mv temp calc_grm.inp
sbatch run_calc_grm.sh


cd ../../../../r_sequence
awk 'NR==FNR{a[$0]=$0}NR>FNR{if($1==a[$1])print $0}' num.txt p1_qtl_001.txt > QTLgeno.txt
mv QTLgeno.txt ./QTL

cd QTL
cp /home/WUR/yang110/files/QTL/* .

var=$(echo | tail -n1 var_sorted_cumm.txt | awk '{print $3}')
awk -v var1="$var" '{print $1,$2,$3,(100/var1)*$3}' var_sorted_cumm.txt > var_sorted_cumm_perc.txt
#clean up
rm var_sorted_cumm.txt var_sorted.txt
#replace alleles by genotypes (i.e. sum 2 adjacent columns, and subtract 2)
awk '{print $1}' QTLgeno.txt > IDs
awk -f alleles2genotypes.awk < QTLgeno.txt > QTL_genotypes.txt
paste IDs QTL_genotypes.txt > IDs_QTL_genotypes.txt

#run program to reorder QTL genotypes, such that the one in the first column explains most variance
./reorder_QTL_genotypes
#clean up
rm IDs_QTL_genotypes.txt IDs QTL_genotypes.txt
awk '{print $4}' var_sorted_cumm_perc.txt > detect.txt
mv IDs_QTL_genotypes_sorted.txt genotype.txt


mkdir 0
mkdir 0.05
mkdir 0.1
mkdir 0.15
mkdir 0.2
mkdir 0.4
mkdir 0.6
mkdir 0.7
mkdir 0.8
mkdir 0.85
mkdir 0.9
mkdir 0.95
mkdir 0.99
mkdir 1
cp /home/WUR/yang110/files/0/* ./0
cp /home/WUR/yang110/files/0.05/* ./0.05
cp /home/WUR/yang110/files/0.1/* ./0.1
cp /home/WUR/yang110/files/0.15/* ./0.15
cp /home/WUR/yang110/files/0.2/* ./0.2
cp /home/WUR/yang110/files/0.4/* ./0.4
cp /home/WUR/yang110/files/0.6/* ./0.6
cp /home/WUR/yang110/files/0.7/* ./0.7
cp /home/WUR/yang110/files/0.8/* ./0.8
cp /home/WUR/yang110/files/0.85/* ./0.85
cp /home/WUR/yang110/files/0.9/* ./0.9
cp /home/WUR/yang110/files/0.95/* ./0.95
cp /home/WUR/yang110/files/0.99/* ./0.99
cp /home/WUR/yang110/files/1/* ./1

#make calc_grm.inp files & copy to the directories - Changed by Mario Calus 14-12-2021
/home/WUR/yang110/programs/make_calc_grm_inp
mv calc_grm_005.inp ./0.05/calc_grm.inp
mv calc_grm_010.inp ./0.1/calc_grm.inp
mv calc_grm_015.inp ./0.15/calc_grm.inp
mv calc_grm_020.inp ./0.2/calc_grm.inp
mv calc_grm_040.inp ./0.4/calc_grm.inp
mv calc_grm_060.inp ./0.6/calc_grm.inp
mv calc_grm_070.inp ./0.7/calc_grm.inp
mv calc_grm_080.inp ./0.8/calc_grm.inp
mv calc_grm_085.inp ./0.85/calc_grm.inp
mv calc_grm_090.inp ./0.9/calc_grm.inp
mv calc_grm_095.inp ./0.95/calc_grm.inp
mv calc_grm_099.inp ./0.99/calc_grm.inp
mv calc_grm_100.inp ./1/calc_grm.inp

#make TBV
cd ../
mkdir TBV
awk '{print $1,$13}' p1_data_001.txt > T.txt
awk 'NR==FNR{a[$0]=$0}NR>FNR{if($1==a[$1])print $0}' num.txt T.txt > TBV.txt
mv TBV.txt ./TBV
cd TBV
awk '{print $2}' TBV.txt > T1.txt
rm TBV.txt
mv T1.txt TBV.txt

head -12500  TBV.txt > TBV_train.txt
tail -2500 TBV.txt > TBV1.txt
rm TBV.txt
head -500 TBV1.txt > TBV16.txt
tail -500 TBV1.txt > TBV20.txt
tail -2000 TBV1.txt > TBV1720.txt
head -500 TBV1720.txt > TBV17.txt
tail -1500 TBV1.txt > TBV1820.txt
head -500 TBV1820.txt > TBV18.txt
tail -1000 TBV1.txt > TBV1920.txt
head -500 TBV1920.txt > TBV19.txt
rm TBV1720.txt TBV1820.txt TBV1920.txt
mv TBV1.txt TBV.txt


#make phenotype files

cd ../
mkdir phenotype
mv phenotype.txt ./phenotype
mv phenotype_train.txt ./phenotype
cd phenotype

awk '{print $2}' phenotype_train.txt > pheno_train.txt
awk '{print $2}' phenotype.txt > pheno.txt
rm phenotype.txt

head -500 pheno.txt > pheno16.txt
tail -500 pheno.txt > pheno20.txt
tail -2000 pheno.txt > pheno1720.txt
head -500 pheno1720.txt > pheno17.txt
tail -1500 pheno.txt > pheno1820.txt
head -500 pheno1820.txt > pheno18.txt
tail -1000 pheno.txt > pheno1920.txt
head -500 pheno1920.txt > pheno19.txt
rm pheno1720.txt pheno1820.txt pheno1920.txt





























