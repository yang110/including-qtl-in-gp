#!/bin/sh
#SBATCH --time=0-6:00:00
#SBATCH --mem=60000
#SBATCH --cpus-per-task=1

cd r_sequence/QTL

cat genotype.txt | cut -c10- > geno_noID.txt
sed -i 's/ //g' geno_noID.txt 
mv geno_noID.txt ../

cd ../
paste -d '' genotype.txt geno_noID.txt > geno_com.txt

cd QTL

for i in 0.05 0.1 0.15 0.2 0.4 0.6 0.7 0.8 0.85 0.9 0.95 0.99 1_2 
do
cd ${i}
cp /home/WUR/yang110/files/combine/run_com.sh .
cp /home/WUR/yang110/files/combine/A.txt .
sbatch run_com.sh
cd ../
done








