program make_calc_grm_inp
implicit none
integer::SNP,i,j,io,k
integer,dimension(13)::pct,nSNP
real::pve !percentage of variance explained
real,dimension(2)::dummy
character(len=16)::inp_file
pct=(/5,10,15,20,40,60,70,80,85,90,95,99,100/)
open(1,file='var_sorted_cumm_perc.txt')
i=1
j=0
nSNP=0
do
 read(1,*,iostat=io)SNP,dummy(1:2),pve
 if(io/=0)exit
 j=j+1
 if(int(pve)>(pct(i)-1))then
  !store this number of SNPs
  nSNP(i)=j
  !move to the next percentage
  i=i+1
 endif
enddo
close(1)
!now make all calc_grm.inp files
do k=1,13
 inp_file='calc_grm_000.inp'
 write(inp_file(10:12),'(i3.3)')pct(k)
 open(2,file=inp_file)
 write(2,'(i0)')nSNP(k)
 write(2,'(a)')"genotype.txt"
 write(2,'(a)')"genotypes"
 write(2,'(a)')"1"
 write(2,'(a)')"vanraden"
 write(2,'(a)')"giv 0.005"
 if(k==13)then
  write(2,'(a)')"G MIXBLUP"
 else
  write(2,'(a)')"G ASReml"
 endif
 write(2,'(a)')"print_giv=asc"
 write(2,'(a)')"print_geno=no"
 write(2,'(a)')"1"
 write(2,'(a)')"!nohighgrm"
 write(2,'(a)')"!noscale"
 write(2,'(a)')"!epsilon 0.01"
enddo
end program make_calc_grm_inp
