program reorder_QTL_geno
implicit none
integer::n,j,nQ,io,id
integer,allocatable,dimension(:)::order,geno
!read in file with accumulated variances
open(1,file='var_sorted_cumm_perc.txt')
nQ=0
do
 read(1,*,iostat=io)j
 if(io/=0)exit
 nQ=nQ+1
enddo
rewind(1)
print *,nQ,' QTL detected'
allocate(order(nQ))
do j=1,nQ
 read(1,*)order(j)
enddo
close(1)
!read in file with QTL genotypes and re-order columns
open(2,file='IDs_QTL_genotypes.txt')
open(3,file='IDs_QTL_genotypes_sorted.txt')
allocate(geno(nQ))
n=0
do
 read(2,*,iostat=io)id,geno(1:nQ)
 if(io/=0)exit
 n=n+1
 write(3,'(i8,20000i2)')id,geno(order(1:nQ))
enddo
print *,n,' animals detected in file IDs_QTL_genotypes.txt'
close(2)
close(3)
end program reorder_QTL_geno
