#!/bin/sh
#SBATCH --time=0-12:00:00
#SBATCH --mem=60000
#SBATCH --cpus-per-task=1

mkdir w2GBLUP
cp GBLUP.as ./w2GBLUP
cp /home/WUR/yang110/files/w_2GBLUP/calc_grm.inp ./w2GBLUP
QTL=$(head -n 1 calc_grm.inp)
cd w2GBLUP
awk -v mrk_num="$QTL" 'NR==1{$1=mrk_num}1' calc_grm.inp > temp && mv temp calc_grm.inp

ln -s ../../genotype.txt
ln -s ../../var.txt
ln -s ../../../phenotype.txt
ln -s ../../big/ASReml/G2_asreml.giv

calc_grm  
rm G.grm

/cm/shared/apps/WUR/ABGC/asreml4.2.1.45/bin/asreml.sh -NRS20000 GBLUP.as


awk '{print$3}' GBLUP.sln > ebv.txt
tail -30000 ebv.txt > ebv1.txt
rm ebv.txt
mv ebv1.txt ebv.txt
head -15000 ebv.txt > ebv_1.txt
tail -15000 ebv.txt > ebv_2.txt
paste ebv_1.txt ebv_2.txt > ebv_3.txt
awk '{print $1,$2, $1+$2}' ebv_3.txt > ebv_4.txt
awk '{print $3}' ebv_4.txt > ebv_5.txt
rm ebv_1.txt ebv_2.txt ebv_3.txt ebv_4.txt
mv ebv_5.txt ebv.txt

head -12500 ebv.txt > ebv_train.txt
tail -2500 ebv.txt > ebv1.txt
rm ebv.txt
head -500 ebv1.txt > ebv16.txt
tail -500 ebv1.txt > ebv20.txt
tail -2000 ebv1.txt > ebv1720.txt
head -500 ebv1720.txt > ebv17.txt
tail -1500 ebv1.txt > ebv1820.txt
head -500 ebv1820.txt > ebv18.txt
tail -1000 ebv1.txt > ebv1920.txt
head -500 ebv1920.txt > ebv19.txt
rm ebv1720.txt ebv1820.txt ebv1920.txt
mv ebv1.txt ebv.txt

mkdir calculation
mv ebv* ./calculation
cp ../../../TBV/* ./calculation
cp ../../../phenotype/* ./calculation
cd calculation


paste ebv16.txt TBV16.txt > com16TBV.txt
paste ebv17.txt TBV17.txt > com17TBV.txt
paste ebv18.txt TBV18.txt > com18TBV.txt
paste ebv19.txt TBV19.txt > com19TBV.txt
paste ebv20.txt TBV20.txt > com20TBV.txt
paste ebv_train.txt TBV_train.txt > comtrainTBV.txt
paste ebv.txt TBV.txt > comTBV.txt


paste ebv16.txt pheno16.txt > com16pheno.txt
paste ebv17.txt pheno17.txt > com17pheno.txt
paste ebv18.txt pheno18.txt > com18pheno.txt
paste ebv19.txt pheno19.txt > com19pheno.txt
paste ebv20.txt pheno20.txt > com20pheno.txt
paste ebv_train.txt pheno_train.txt > comtrainpheno.txt
paste ebv.txt pheno.txt > compheno.txt

paste pheno_train.txt TBV_train.txt > com_train_pheno_TBV.txt


echo " The correlation between ebv and TBV in generation 16 is"
awk -f /home/WUR/yang110/files/correlation.awk com16TBV.txt

echo " The correlation between ebv and TBV in generation 17 is"
awk -f /home/WUR/yang110/files/correlation.awk com17TBV.txt

echo " The correlation between ebv and TBV in generation 18 is"
awk -f /home/WUR/yang110/files/correlation.awk com18TBV.txt

echo " The correlation between ebv and TBV in generation 19 is"
awk -f /home/WUR/yang110/files/correlation.awk com19TBV.txt

echo " The correlation between ebv and TBV in generation 20 is"
awk -f /home/WUR/yang110/files/correlation.awk com20TBV.txt

echo " The correlation between ebv and TBV in training dataset is"
awk -f /home/WUR/yang110/files/correlation.awk comtrainTBV.txt

echo " The correlation between ebv and TBV in test dataset is"
awk -f /home/WUR/yang110/files/correlation.awk comTBV.txt

echo " The correlation between ebv and phenotype in generation 16 is"
awk -f /home/WUR/yang110/files/correlation.awk com16pheno.txt

echo " The correlation between ebv and phenotype in generation 17 is"
awk -f /home/WUR/yang110/files/correlation.awk com17pheno.txt

echo " The correlation between ebv and phenotype in generation 18 is"
awk -f /home/WUR/yang110/files/correlation.awk com18pheno.txt

echo " The correlation between ebv and phenotype in generation 19 is"
awk -f /home/WUR/yang110/files/correlation.awk com19pheno.txt

echo " The correlation between ebv and phenotype in generation 20 is"
awk -f /home/WUR/yang110/files/correlation.awk com20pheno.txt

echo " The correlation between ebv and phenotype in training dataset is"
awk -f /home/WUR/yang110/files/correlation.awk comtrainpheno.txt

echo " The correlation between ebv and phenotype in test dataset is"
awk -f /home/WUR/yang110/files/correlation.awk compheno.txt


echo " The correlation between TBV and phenotype in training dataset is"
awk -f /home/WUR/yang110/files/correlation.awk com_train_pheno_TBV.txt


echo " The bias of ebv in generation 16 is"
awk -f /home/WUR/yang110/files/regression.awk com16TBV.txt

echo " The bias of ebv in generation 17 is"
awk -f /home/WUR/yang110/files/regression.awk com17TBV.txt

echo " The bias of ebv in generation 18 is"
awk -f /home/WUR/yang110/files/regression.awk com18TBV.txt

echo " The bias of ebv in generation 19 is"
awk -f /home/WUR/yang110/files/regression.awk com19TBV.txt

echo " The bias of ebv in generation 20 is"
awk -f /home/WUR/yang110/files/regression.awk com20TBV.txt

echo " The bias of ebv in training dataset is"
awk -f /home/WUR/yang110/files/regression.awk comtrainTBV.txt

echo " The bias of ebv in test dataset is"
awk -f /home/WUR/yang110/files/regression.awk comTBV.txt


echo " The MSE of ebv in generation 16 is"
awk -f /home/WUR/yang110/files/MSE.awk com16pheno.txt

echo " The MSE of ebv in generation 17 is"
awk -f /home/WUR/yang110/files/MSE.awk com17pheno.txt

echo " The MSE of ebv in generation 18 is"
awk -f /home/WUR/yang110/files/MSE.awk com18pheno.txt

echo " The MSE of ebv in generation 19 is"
awk -f /home/WUR/yang110/files/MSE.awk com19pheno.txt

echo " The MSE of ebv in generation 20 is"
awk -f /home/WUR/yang110/files/MSE.awk com20pheno.txt

echo " The MSE of ebv in training dataset is"
awk -f /home/WUR/yang110/files/MSE.awk comtrainpheno.txt

echo " The MSE of ebv in test dataset is"
awk -f /home/WUR/yang110/files/MSE.awk compheno.txt

cd ../../
latest_file=$(ls -t slurm-*.out | head -1)
mv "$latest_file" ./w2GBLUP

